/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <getopt.h>
#include <libgen.h>
#include <pwd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "oauth2.h"

static char *progname = NULL;

static struct option opts[] = {
	{"scope", required_argument, NULL, 's'},
	{"token", required_argument, NULL, 't'},
	{"type", required_argument, NULL, 'T'},
	{"username", required_argument, NULL, 'u'},
	{"verbose", no_argument, NULL, 'v'},
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'V'},
	{NULL, 0, NULL, 0}
};

static struct option_help {
	char *name;
	char *def;
	char *only;
}	helps[] = {
	{"scope", NULL, NULL},
	{"refresh token", NULL, "refresh"},
	{"type", NULL, NULL}, /*Special*/
	{"user name", "current user", "password"},
	{"be verbose", NULL, NULL},
	{"show help and exit", NULL, NULL},
	{"show version and exit", NULL, NULL},
	{NULL, NULL, NULL}
};

static void usage(void)
{
	int i;

	fprintf(stderr, "usage: %s --type=[password|client|refresh] [options]\n",
				progname);
	for (i = 0; opts[i].name; i++) {
		if (strcmp(opts[i].name, "type") != 0)
			fprintf(stderr, "\t--%s%s%c: %s%c%c%s%c%s%s%s\n",
					opts[i].name,
					opts[i].val <= 127 ? ", -" : "",
					opts[i].val <= 127 ? opts[i].val  : '\0',
					helps[i].name,
					helps[i].def ? ' ' : '\0',
					helps[i].def ? '(' : '\0',
					helps[i].def ? helps[i].def : "",
					helps[i].def ? ')' : '\0',
					helps[i].only ? ", " : "",
					helps[i].only ? helps[i].only : "",
					helps[i].only ? " only." : "");
	}
	exit(EXIT_FAILURE);
}

static char *get_client_secret(const char *fname)
{
	FILE *f;
	static char buf[1024];
	int i;

	if (!(f = fopen(fname, "r")))
		return NULL;
	if (fread(buf, 1, sizeof(buf), f) == 0)
		return NULL;
	for (i = strlen(buf)-1; i > 0; i--)
		if (buf[i] == '\n')
			buf[i] = '\0';
	fclose(f);
	return buf;
}

int main(int argc, char **argv)
{
	int c, verbose = 0;
	char *type = NULL,
			 *client_id = NULL, *sf = NULL, *client_secret = NULL,
			 *username = NULL, *password = NULL, *token = NULL, *scope = NULL;
	struct oauth2 *o = NULL;
	struct passwd *pwd;
	char fname[1024];

	progname = basename(argv[0]);

	for (;;) {
		int index = 0;

		c = getopt_long(argc, argv, "c:C:hs:t:T:u:vV", opts, &index);
		if (c < 0)
			break;

		switch (c) {
			case 's':
				if (!scope)
					scope = optarg;
				else
					usage();
				break;
			case 't':
				if (!token)
					token = optarg;
				else
					usage();
				break;
			case 'T':
				if (!type) {
					type = optarg;
					if (!strcmp(type, "password") == 0 &&
							!strcmp(type, "client") == 0 &&
							!strcmp(type, "refresh") ==0)
						usage();
				}
				else
					usage();
				break;
			case 'u':
				if (!username)
					username = optarg;
				else
					usage();
				break;
			case 'v':
				verbose++;
				break;
			case 'V':
				printf("%s, part of %s\n\n", progname, PACKAGE_STRING);
				printf("Copyright (C) 2016,  Éric Dorino\n");
				printf(
					"This is free software; see the source "
					"for copying conditions.  There is NO\n"
					"warranty; not even for MERCHANTABILITY "
					"of FITNESS FOR A PARTICULAR PURPOSE.\n");
				return EXIT_SUCCESS;
			case 'h':
			default:
				usage();
		}
	}

	if (!type) {
		usage();
		return EXIT_FAILURE;
	}
	if (((strcmp(type, "password") == 0 || strcmp(type, "client") == 0 ) &&
					token) ||
			((strcmp(type, "client") == 0 || strcmp(type, "refresh") == 0) && 
			 		username)) {
		usage();
		return EXIT_FAILURE;
	}

	if (strcmp(type, "password") == 0) {
		client_id = "liboauth2_password";
		sf = "/var/www/oauth2/.password_secret";
	}
	else if (strcmp(type, "client") == 0) {
		client_id = "liboauth2_client";
		sf = "/var/www/oauth2/.client_secret";
	}
	else { /* type == refresh */
		client_id = "liboauth2_password";
		sf = "/var/www/oauth2/.password_secret";
	}

	if (!(client_secret = get_client_secret(sf))) {
		fprintf(stderr, "%s: cannot read client_secret\n", progname);
		return EXIT_FAILURE;
	}

	if (strcmp(type, "password") == 0) {
		if (!username) {
			if (!(pwd = getpwuid(getuid()))) {
				fprintf(stderr, "%s: missing username\n", progname);
				return EXIT_FAILURE;
			}
			username = pwd->pw_name;
		}
		if (!(password =getpass("password: "))) {
			fprintf(stderr, "%s: cannot get password\n", progname);
			return EXIT_FAILURE;
		}
	}
	else if (strcmp(type, "resfresh") == 0) {
		if (!token) {
			fprintf(stderr, "%s: missing token\n", progname);
			return EXIT_FAILURE;
		}
	}

	if (!(o = oauth2_create("https://localhost:8443/oauth2/token",
									client_id, client_secret, verbose))) {
		fprintf(stderr, "%s: cannot create endpoint\n", progname);
		return EXIT_FAILURE;
	}

	if (strcmp(type, "password") == 0) {
		if (!oauth2_password(o, username, password, scope)) {
			fprintf(stderr, "%s: cannot auth: %s\n", progname, oauth2_get_error(o));
			return EXIT_FAILURE;
		}
	}
	else if (strcmp(type, "refresh") == 0) {
		if (!oauth2_refresh(o, token, scope)) {
			fprintf(stderr, "%s: cannot auth: %s\n", progname, oauth2_get_error(o));
			return EXIT_FAILURE;
		}
	}
	else {
		if (!oauth2_client_credentials(o, scope)) {
			fprintf(stderr, "%s: cannot auth: %s\n", progname, oauth2_get_error(o));
			return EXIT_FAILURE;
		}
	}

	printf("token: \"%s\"\ntoken_type: \"%s\"\nexpires_in: %ld\n",
		oauth2_get_token(o),
		oauth2_get_token_type(o),
		oauth2_get_token_expires_in(o));
	if (oauth2_get_token_scope(o))
		printf("scope: \"%s\"\n", oauth2_get_token_scope(o));
	if (oauth2_get_refresh_token(o))
		printf("refresh_token: \"%s\"\n", oauth2_get_refresh_token(o));

	oauth2_delete(o);

	return EXIT_SUCCESS;
}

