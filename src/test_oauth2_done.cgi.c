/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "oauth2.h"

static int find_query_string(const char *query, const char *name,
								char **begin, char **end)
{
	const char *q = query;
	char *sep, *eq;

	if (!q) {
		*begin = *end = NULL;
		return 0;
	}

	while ((sep = strchrnul(q, '&')) || (sep = strchrnul(q, ';'))) {
		if ((eq = strchrnul(q, '=')) && strncasecmp(name, q, eq - q) == 0) {
			if (*eq) {
				*begin = eq + 1;
				*end = sep;
			}
			else
				*begin = *end = "";
			return 1;
		}
		if (*sep == '\0')
			break;
		else q = sep + 1;
	}
	*begin = *end = NULL;
	return 0;
}

static char *get_client_secret(const char *fname)
{
	FILE *f;
	static char buf[1024];
	int i;

	if (!(f = fopen(fname, "r")))
		return NULL;
	if (fread(buf, 1, sizeof(buf), f) == 0)
		return NULL;
	for (i = strlen(buf)-1; i > 0; i--)
		if (buf[i] == '\n')
			buf[i] = '\0';
	fclose(f);
	return buf;
}

int main(int argc, char **argv)
{
	int verbose = 0;
	char *client_secret, 
			 *code = NULL,
			 *state = "123",
			 *error = NULL,
			 *redirect_uri = "https://localhost:8443/test_oauth2_done.cgi",
			 *p, *q;
	struct oauth2 *o = NULL;

	if (find_query_string(getenv("QUERY_STRING"), "code", &p, &q)) {
		code = strndup(p, q-p);
	}
	if (find_query_string(getenv("QUERY_STRING"), "state", &p, &q)) {
		state = strndup(p, q-p);
	}
	if (find_query_string(getenv("QUERY_STRING"), "error", &p, &q)) {
		error = strndup(p, q-p);
	}

	client_secret = get_client_secret("/var/www/oauth2/.code_secret");

	if (code) {
		if (!(o = oauth2_create("https://localhost:8443/oauth2/token",
									"liboauth2_code", client_secret, verbose))) {
			puts("Status: 500 Endpoint Error\n");
			return EXIT_SUCCESS;
		}

		if (!oauth2_code_token(o, code, redirect_uri, state)) {
			printf("Status: 500 Internal Server Error (%s)\n\n",
											oauth2_get_error(o));
			return EXIT_SUCCESS;
		}

		printf("Status: 200 OK\nContent-Type: text/plain\n\n"
					 "token: \"%s\"\ntoken_type: %s\nexpires_in: %ld\n"
					 "refresh_token: \"%s\"\n",
			oauth2_get_token(o),
			oauth2_get_token_type(o),
			oauth2_get_token_expires_in(o),
			oauth2_get_refresh_token(o));

		oauth2_delete(o);
	}
	else {
		printf("Status: 200 OK\nContent-Type: text/plain\n\nerror : \"%s\"\n",
					error);
	}
	return EXIT_SUCCESS;
}

