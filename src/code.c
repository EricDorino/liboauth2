/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <json.h>
#include <stdlib.h>
#include <string.h>
#include "internal.h"

static size_t header_fun(void *ptr, size_t size, size_t nmemb, void *userdata)
{
	char *begin, *end;

	if (strncmp((char *)ptr, "Location: ", 10) == 0) {
		begin = (char *)ptr+10;
		if ((end = strchr(begin, '\r')))
			*((char **)userdata) = strndup(begin, end-begin);
	}
	return size*nmemb;
}

static int find_query_string(const char *query, const char *name,
								char **begin, char **end)
{
	const char *q = query;
	char *sep, *eq;

	if (!q) {
		*begin = *end = NULL;
		return 0;
	}

	while ((sep = strchrnul(q, '&')) || (sep = strchrnul(q, ';'))) {
		if ((eq = strchrnul(q, '=')) && strncasecmp(name, q, eq - q) == 0) {
			if (*eq) {
				*begin = eq + 1;
				*end = sep;
			}
			else
				*begin = *end = "";
			return 1;
		}
		if (*sep == '\0')
			break;
		else q = sep + 1;
	}
	*begin = *end = NULL;
	return 0;
}

int oauth2_code_grant(struct oauth2 *o,
								const char *redirect_uri,
								const char *scope)
{
	char postfields[2048];
	long rc;
	char buf[255], erbuf[255];
	struct writedata writedata;

	if (!o)
		return 0;

	if (o->verbose)
		fprintf(stderr, "oauth2_code_grant(%s, %s)\n",
										redirect_uri, scope);

	o->erbuf[0] = '\0';
	buf[0] = '\0';
	writedata.buf = buf;
	writedata.bufsize = sizeof(buf);

	o->state = strdup("123"); /*FIXME*/

	snprintf(postfields, sizeof(postfields),
					 "response_type=code&client_id=%s%s%s%s%s&state=%s",
					 o->client_id,
					 scope ? "&scope=" : "",
					 scope ? scope : "",
					 redirect_uri ? "&redirect_uri=" : "",
					 redirect_uri ? redirect_uri : "",
					 o->state);

	if (o->client_secret) {
		curl_easy_setopt(o->curl, CURLOPT_USERNAME, o->client_id);
		curl_easy_setopt(o->curl, CURLOPT_PASSWORD, o->client_secret);
	}
	curl_easy_setopt(o->curl, CURLOPT_POST, 1);
	curl_easy_setopt(o->curl, CURLOPT_POSTFIELDS, postfields);
	curl_easy_setopt(o->curl, CURLOPT_HEADERFUNCTION, header_fun);
	curl_easy_setopt(o->curl, CURLOPT_HEADERDATA, &o->redirect_uri);
	curl_easy_setopt(o->curl, CURLOPT_WRITEFUNCTION, write_fun);
	curl_easy_setopt(o->curl, CURLOPT_WRITEDATA, &writedata);
	
	if (curl_easy_perform(o->curl) > 0 || 
			curl_easy_getinfo(o->curl, CURLINFO_RESPONSE_CODE, &rc))
		return 0;

	switch (rc) {
		case 302:
			return 1;
		case 400:
		case 500:
			if (!(o->response = json_from_buf(
												writedata.buf, strlen(writedata.buf),
											 	erbuf, sizeof(erbuf)))) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Invalid response: %s", erbuf);
			}	
			else if (!json_find_string(o->response, "error", &o->error)) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Unspecified");
			}
			else
				snprintf(o->erbuf, sizeof(o->erbuf), "%s", o->error);
			return 0;
		default:
			snprintf(o->erbuf, sizeof(o->erbuf), "Invalid code %ld", rc);
			return 0;
	}
	return 1;
}

int oauth2_code_token(struct oauth2 *o,
								const char *code,
								const char *redirect_uri,
								const char *state)
{
	char postfields[2048];
	long rc;
	char buf[255], erbuf[255];
	struct writedata writedata;
	char *p, *b, *e;

	if (!o)
		return 0;

	o->erbuf[0] = '\0';
	buf[0] = '\0';
	writedata.buf = buf;
	writedata.bufsize = sizeof(buf);

	if (state && o->state && strcmp(state, o->state) != 0) {
		snprintf(o->erbuf, sizeof(o->erbuf), "Unmatching state");
		return 0;
	}

	snprintf(postfields, sizeof(postfields),
					 "grant_type=authorization_code&code=%s%s%s%s%s",
					 code,
					 !o->client_secret ? "&client_id=" : "",
					 !o->client_secret ? o->client_id : "",
					 redirect_uri ? "&redirect_uri=" : "",
					 redirect_uri ? redirect_uri : "");

	if (o->client_secret) {
		curl_easy_setopt(o->curl, CURLOPT_USERNAME, o->client_id);
		curl_easy_setopt(o->curl, CURLOPT_PASSWORD, o->client_secret);
	}
	curl_easy_setopt(o->curl, CURLOPT_POST, 1);
	curl_easy_setopt(o->curl, CURLOPT_POSTFIELDS, postfields);
	curl_easy_setopt(o->curl, CURLOPT_WRITEFUNCTION, write_fun);
	curl_easy_setopt(o->curl, CURLOPT_WRITEDATA, &writedata);
	
	if (curl_easy_perform(o->curl) > 0 || 
			curl_easy_getinfo(o->curl, CURLINFO_RESPONSE_CODE, &rc))
		return 0;

	switch (rc) {
		case 200:
			if (!(o->response = json_from_buf(
												writedata.buf, strlen(writedata.buf),
											 	erbuf, sizeof(erbuf)))) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Invalid response: %s", erbuf);
				return 0;
			}	
			if (!(json_find_string(o->response, "access_token", &o->token) &&
						json_find_string(o->response, "token_type", &o->token_type))) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Invalid response data");
				return 0;
			}	
			json_find_string(o->response, "refresh_token", &o->refresh_token);
			json_find_int(o->response, "expires_in", &o->expires_in);
			break;
		case 400:
		case 500:
			if (!(o->response = json_from_buf(
												writedata.buf, strlen(writedata.buf),
											 	erbuf, sizeof(erbuf)))) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Invalid response: %s", erbuf);
				return 0;
			}	
			if (!json_find_string(o->response, "error", &o->error)) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Unspecified");
				return 0;
			}
			snprintf(o->erbuf, sizeof(o->erbuf), "%s", o->error);
			return 0;
		default:
			snprintf(o->erbuf, sizeof(o->erbuf), "Invalid code %ld", rc);
			return 0;
	}
	return 1;
}
