/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef oauth2_h_
#define oauth2_h_

struct oauth2;

extern struct oauth2 *oauth2_create(const char *,
																		const char *client_id,
																		const char *client_secret,
																		int verbose);

extern void oauth2_delete(struct oauth2 *);

extern int oauth2_error(struct oauth2 *);
extern const char *oauth2_get_error(struct oauth2 *);

extern const char *oauth2_get_token(struct oauth2 *);
extern const char *oauth2_get_refresh_token(struct oauth2 *);
extern const char *oauth2_get_token_type(struct oauth2 *);
extern long oauth2_get_token_expires_in(struct oauth2 *);
extern const char *oauth2_get_token_scope(struct oauth2 *);

extern int oauth2_code_grant(struct oauth2 *,
														const char *redirect_uri,
														const char *scope);
extern const char *oauth2_get_redirect_uri(struct oauth2 *);

extern int oauth2_code_token(struct oauth2 *,
														const char *grant, 
														const char *redirect_uri,
														const char *state);

extern int oauth2_implicit(struct oauth2 *,
														const char *redirect_uri,
														const char *scope);

extern int oauth2_password(struct oauth2 *,
													 const char *username,
													 const char *password,
													 const char *scope);

extern int oauth2_client_credentials(struct oauth2 *,
													 const char *scope);

extern int oauth2_refresh(struct oauth2 *,
													const char *token,
													const char *scope);

#endif
