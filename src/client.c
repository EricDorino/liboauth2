/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <json.h>
#include <stdlib.h>
#include <string.h>
#include "internal.h"

int oauth2_client_credentials(struct oauth2 *o, const char *scope)
{
	char postfields[2048];
	long rc;
	char buf[255], erbuf[255];
	struct writedata writedata;

	if (!o)
		return 0;

	if (o->verbose)
		fprintf(stderr, "oauth2_client_credentials(%s)\n", scope);

	o->erbuf[0] = '\0';
	buf[0] = '\0';
	writedata.buf = buf;
	writedata.bufsize = sizeof(buf);

	snprintf(postfields, sizeof(postfields),
					 "grant_type=client_credentials%s%s%s%s",
					 !o->client_secret ? "&client_id=" : "",
					 !o->client_secret ? o->client_id : "",
					 scope ? "&scope=" : "",
					 scope ? scope : "");

	if (o->client_secret) {
		curl_easy_setopt(o->curl, CURLOPT_USERNAME, o->client_id);
		curl_easy_setopt(o->curl, CURLOPT_PASSWORD, o->client_secret);
	}
	curl_easy_setopt(o->curl, CURLOPT_POST, 1);
	curl_easy_setopt(o->curl, CURLOPT_POSTFIELDS, postfields);
	curl_easy_setopt(o->curl, CURLOPT_WRITEFUNCTION, write_fun);
	curl_easy_setopt(o->curl, CURLOPT_WRITEDATA, &writedata);
	
	if (curl_easy_perform(o->curl) > 0 || 
			curl_easy_getinfo(o->curl, CURLINFO_RESPONSE_CODE, &rc))
		return 0;

	switch (rc) {
		case 200:
			if (!(o->response = json_from_buf(
												writedata.buf, strlen(writedata.buf),
											 	erbuf, sizeof(erbuf)))) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Invalid response: %s", erbuf);
				return 0;
			}	
			if (!(json_find_string(o->response, "access_token", &o->token) &&
						json_find_string(o->response, "token_type", &o->token_type))) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Invalid response data");
				return 0;
			}
			json_find_int(o->response, "expires_in", &o->expires_in);
			json_find_string(o->response, "scope", &o->scope);
			break;
		case 400:
		case 500:
			if (!(o->response = json_from_buf(
												writedata.buf, strlen(writedata.buf),
											 	erbuf, sizeof(erbuf)))) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Invalid response: %s", erbuf);
				return 0;
			}	
			if (!json_find_string(o->response, "error", &o->error)) {
				snprintf(o->erbuf, sizeof(o->erbuf), "Unspecified");
				return 0;
			}
			snprintf(o->erbuf, sizeof(o->erbuf), "%s", o->error);
			return 0;
		default:
			snprintf(o->erbuf, sizeof(o->erbuf), "Invalid code %ld", rc);
			return 0;
	}
	return 1;
}

