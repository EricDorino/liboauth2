/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef internal_h_
#define internal_h_
#include <curl/curl.h>
#include "oauth2.h"

struct oauth2 {
	int verbose;
	CURL *curl;
	char erbuf[CURL_ERROR_SIZE];
	char *client_id,
			 *client_secret,
			 *redirect_uri,
			 *state;
	const char *error,
			 *scope,
			 *grant,
			 *token,
			 *token_type,
			 *refresh_token;
	long expires_in;
	struct json *response;
};

struct writedata {
	size_t bufsize;
	char *buf;
};

extern size_t write_fun(void *, size_t , size_t, void *);

#endif
