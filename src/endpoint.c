/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <json.h>
#include <stdlib.h>
#include <string.h>
#include "internal.h"

struct oauth2 *oauth2_create(const char *endpoint,
														 const char *client_id,
														 const char *client_secret,
														 int verbose)
{
	struct oauth2 *result = malloc(sizeof(*result));

	if (verbose)
		fprintf(stderr, "oauth2_create [%s][%s][%s]\n",
										endpoint, client_id, client_secret ? "*" : "");

	if (!result)
		return NULL;

	memset(result, 0, sizeof(*result));

	result->verbose = verbose;

	if (!(result->curl = curl_easy_init()))
		return NULL;

	curl_easy_setopt(result->curl, CURLOPT_VERBOSE, verbose);
	curl_easy_setopt(result->curl, CURLOPT_URL, endpoint);
	curl_easy_setopt(result->curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_easy_setopt(result->curl, CURLOPT_USERAGENT, "liboauth/" VERSION);
	curl_easy_setopt(result->curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(result->curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(result->curl, CURLOPT_ERRORBUFFER, result->erbuf);

	result->client_id = client_id ? strdup(client_id) : NULL;
	result->client_secret = client_secret ? strdup(client_secret) : NULL; 

	return result;
}

void oauth2_delete(struct oauth2 *o)
{
	if (!o)
		return;
	if (o->verbose)
		fprintf(stderr, "oauth2_delete\n");
	curl_easy_cleanup(o->curl);
	free(o->client_id);
	free(o->client_secret);
	free(o->redirect_uri);
	free(o->state);
	json_free(o->response);
}

int oauth2_error(struct oauth2 *o)
{
	return o ? strlen(o->erbuf) > 0 || o->error : 0;
}

const char *oauth2_get_error(struct oauth2 *o)
{
	return o ?
				strlen(o->erbuf) > 0 ?
					o->erbuf : o->error ?
				 		o->error :
						"No error specified" :
				"Invalid endpoint";
}

const char *oauth2_get_token(struct oauth2 *o)
{
	return o ? o->token : NULL;
}

const char *oauth2_get_refresh_token(struct oauth2 *o)
{
	return o ? o->refresh_token : NULL;
}

const char *oauth2_get_token_type(struct oauth2 *o)
{
	return o ? o->token_type : NULL;
}

long oauth2_get_token_expires_in(struct oauth2 *o)
{
	return o ? o->expires_in : 0;
}

const char *oauth2_get_token_scope(struct oauth2 *o)
{
	return o ? o->scope : NULL;
}

const char *oauth2_get_redirect_uri(struct oauth2 *o)
{
	return o ? o->redirect_uri : NULL;
}

size_t write_fun(void *ptr, size_t size, size_t nmemb, void *userdata)
{
	size_t remains = ((struct writedata *)userdata)->bufsize - 
									strlen(((struct writedata *)userdata)->buf) - 1;

	if (remains > size*nmemb)
		strncat(((struct writedata *)userdata)->buf, (char *)ptr, size*nmemb);
	else
		strncat(((struct writedata *)userdata)->buf, (char *)ptr, remains);

	return size*nmemb;
}

