/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "oauth2.h"

int main(int argc, char **argv)
{
	int verbose = 0;
	char *redirect_uri = NULL;
	char *scope = "feeds p2p";
	struct oauth2 *o = NULL;

	if (!(o = oauth2_create("https://localhost:8443/oauth2/authorize",
									"liboauth2_code", NULL, verbose))) {
		puts("Status: 500 Endpoint Error\n");
		return EXIT_SUCCESS;
	}

	if (!oauth2_code_grant(o, redirect_uri, scope)) {
		printf("Status: 500 Internal Server Error (%s)\n\n", oauth2_get_error(o));
		return EXIT_SUCCESS;
	}

	if (!oauth2_get_redirect_uri(o))
		puts("Status: 500 Internal Server Error (No redirection URI)\n");
	else
		printf("Status: 302 Found\nLocation: %s\n\n",
						oauth2_get_redirect_uri(o));

	oauth2_delete(o);

	return EXIT_SUCCESS;
}

